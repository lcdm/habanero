
# TODO and ideas list
#===========================

* Sort article list

* Display all data per article (print change time only if difference is >5min)

* Print pages 1,2,3, ... N on bottom of page

* User login

* When user logged in:

	* show token info (click something to have perma link with token in clipboard)

	* show new article link on top of article link, on click show form

	* add edit link to each article. When clicked, show form with article loaded insteda of the article



* Add article filter logic, see views.py


* Make sure site is usable on all kinds of devices (desktop browser, phone, tablet, text browsers, 1st gen Ipod, tamagochi, ...)
