from django.conf.urls import include, url
from django.contrib import admin
import django.contrib.staticfiles.views
import main_app.views

import habanero.settings


urlpatterns = [
    # Examples:
    # url(r'^$', 'habanero.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^get_articles', main_app.views.get_articles),
    url(r'^new_article', main_app.views.new_article),

    url(r'^init_search', main_app.views.init_search),
    url(r'^execute_search', main_app.views.execute_search),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', django.contrib.staticfiles.views.serve, {'path': '/index.html'}),

]
