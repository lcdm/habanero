from django.db import models
import django.contrib.auth.models

# Create your models here.

class Article(models.Model):

    title        = models.TextField()

    creationDate = models.DateTimeField()
    changeDate   = models.DateTimeField()

    content      = models.TextField() 

    author       = models.ForeignKey(django.contrib.auth.models.User)

    public       = models.BooleanField(default=True)

    token        = models.CharField(max_length=32)


    def __str__(self):
        return self.title
