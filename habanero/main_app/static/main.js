class Search {

    constructor(searchCriterion, onReadyCallback) {
        var self = this;
        $.ajax({url: 'init_search',
                data: { searchCriterion: JSON.stringify(searchCriterion) },
                success: function(data){ self.searchID = data.SearchID; onReadyCallback(); }
                });
    }

    find(lowerLimit, upperLimit, onResultAvailableCallback) {
        $.ajax({url: 'execute_search',
                data: {lowerLimit: lowerLimit, upperLimit: upperLimit, searchID: this.searchID},
                success: onResultAvailableCallback});
    }

}

/* print articles received from backend */
function printResults(articles) {

    var targetNode = document.getElementById("content");
    targetNode.innerHTML = "";


    for(var article of articles){

        var htmlArticleHead = document.createElement("div");
        htmlArticleHead.className = "articleHead";
        htmlArticleHead.textContent = article.fields.title;
        targetNode.appendChild(htmlArticleHead);

        var htmlArticleHeadPanel = document.createElement("div");
        htmlArticleHeadPanel.className = "articleHeadPanel";
        htmlArticleHead.appendChild(htmlArticleHeadPanel);

        var htmlArticleTokenLink = document.createElement("a");
        htmlArticleTokenLink.textContent = "token link";
        htmlArticleTokenLink.href = "?token=" + article.fields.token;
        htmlArticleHeadPanel.appendChild(htmlArticleTokenLink);

        var htmlArticleContent = document.createElement("div");
        htmlArticleContent.className = "articleContent";
        htmlArticleContent.textContent = article.fields.content;

        console.log(htmlArticleContent.childNodes[0].nodeValue.replace(/(?:\r\n|\r|\n)/g, '<br>'));
        htmlArticleContent.textContent = htmlArticleContent.textContent.replace(/(?:\r\n|\r|\n)/g, '<br />');
        /* TODO: this does not work. Instead of the <br> tag,
                 the following is inserted instead &lt;/br&gt;
        */

        targetNode.appendChild(htmlArticleContent);


        var htmlArticleCreationDate = document.createElement("div");
        htmlArticleCreationDate.className = "articleCreationDate";
        htmlArticleCreationDate.textContent = article.fields.creationDate;
        targetNode.appendChild(htmlArticleCreationDate);


        var htmlArticleChangeDate = document.createElement("div");
        htmlArticleChangeDate.className = "articleChangeDate";
        htmlArticleChangeDate.textContent = article.fields.changeDate;
        targetNode.appendChild(htmlArticleChangeDate);

    }
}

/* submit search query to backend */
function submitSearch() {
    var searchFieldValue = document.getElementById('searchField').value;
    var a = new Search({keyword: searchFieldValue}, function(){ a.find(0, 20, printResults); });
    // TODO: 0, 20 should not be hard coded here
}

/* submit the newly written article */
function submitNewArticle() {
    var title = $('#newArticleTitle').val();
    var content = $('#newArticleContent').val();
    var public = $('#newArticlePublic').prop('checked') ? "1" : "0";
    $.ajax({ //TODO make a POST request instead
        url: 'new_article',
        data: {title: title, content: content, public: public},
        success: function(data) {
            submitSearch(); //TODO better way to reload (conserving the old search)
        },
    });
    showNewArticlePanel(false);
}

/* shows the panel for writing new articles */
function showNewArticlePanel(v) {
    var newArticlePanel = $('#newArticlePanel');
    if (v === undefined)
        v = !newArticlePanel.hasClass('visible');  // no parameter was given, so we toggle
    if (v) {
        $('#newArticleTitle').val('');
        $('#newArticleContent').val('');
        $('#newArticlePublic').prop('checked', false);
        newArticlePanel.addClass('visible');
    }
    else
        newArticlePanel.removeClass('visible');
}

/* shows the 'about habanero' info */
function showAbout() {
    document.body.style.transform = (document.body.style.transform.length<5?'rotate(180deg)':'none');
}

/* submit query when enter key is pressed in search field */
$(document).ready(function() {
    $('#searchField').keydown(function(event) {
        if (event.which == 13) {
            submitSearch();
         }
    });
});

/* submit empty search on load */
window.onload = function() {
    var url = new URL(window.location.href);
    var token = url.searchParams.get("token");
    if (token)
        var a = new Search({token: token}, function(){ a.find(0, 20, printResults); });
    else
        var a = new Search({keyword: ""}, function(){ a.find(0, 20, printResults); });
};
