import datetime
import hashlib
import json
import uuid

from django.http import HttpResponse
from django.core.serializers import serialize
import django.utils.timezone as timezone

from main_app.models import Article
import habanero.settings


_searches = {} # global dictionary to (temporarily) store users' search IDs and querries


def index(request):
    return HttpResponse("Hello, world. You're at the main_app index.")


def get_articles(request):

    responseData = serialize('json', Article.objects.all())

    return HttpResponse(responseData, content_type='application/json')


def new_article(request):
    if request.user.is_authenticated():
        title = request.GET['title']
        content = request.GET['content']
        public = request.GET['public'] == "1"
        now = timezone.now()
        newarticle = Article()
        newarticle.title = title
        newarticle.content = content
        newarticle.changeDate = now
        newarticle.creationDate = now
        newarticle.author = request.user
        newarticle.public = public
        newarticle.token = uuid.uuid4().hex
        newarticle.save()
        return HttpResponse("true", content_type='application/json')


def init_search(request):

    searchCriterion = request.GET['searchCriterion']

    searchID = hashlib.sha1(searchCriterion.encode()).hexdigest() #.encode to go from string to byte array

    responseData =  {'SearchID': searchID}

    _searches[searchID] = json.loads(searchCriterion)

    return HttpResponse(json.dumps(responseData), content_type='application/json')


def execute_search(request):
    searchID = request.GET['searchID']
    lowerLimit = request.GET['lowerLimit']
    upperLimit  = request.GET['upperLimit']

    searchCriterion = _searches[searchID]
    result = []
    if "keyword" in searchCriterion:
        result = Article.objects.filter(content__contains=searchCriterion["keyword"])
    elif "token" in searchCriterion:
        result = Article.objects.filter(token=searchCriterion["token"])


            # thoughts on the filter logic
        # obey the following operators: AND, OR, NOT
        # filter for public private

        # lower and upper limit should be applied on a new-to-old-sorted list
        # article objects should be searched in title and content

    return HttpResponse(serialize('json',result), content_type='application/json')
